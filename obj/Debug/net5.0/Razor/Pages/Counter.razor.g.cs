#pragma checksum "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/Pages/Counter.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6e1ced336a08e09e4488a1b536e67391c91e5ece"
// <auto-generated/>
#pragma warning disable 1591
namespace nombre_magique.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/_Imports.razor"
using nombre_magique;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/_Imports.razor"
using nombre_magique.Shared;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/counter")]
    public partial class Counter : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h1>Counter</h1>\r\n\r\n");
            __builder.OpenElement(1, "p");
            __builder.AddContent(2, "Current count: ");
#nullable restore
#line 5 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/Pages/Counter.razor"
__builder.AddContent(3, currentCount);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.AddMarkupContent(4, "\r\n");
            __builder.OpenElement(5, "input");
            __builder.AddAttribute(6, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 6 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/Pages/Counter.razor"
              currentCount

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(7, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => currentCount = __value, currentCount));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(8, "\r\n");
            __builder.OpenElement(9, "button");
            __builder.AddAttribute(10, "class", "btn btn-primary");
            __builder.AddAttribute(11, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 7 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/Pages/Counter.razor"
                                          IncrementCount

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(12, "Click me");
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 9 "/Users/mohamed/Downloads/hts-learning-blazor-server/NombreMagique/Pages/Counter.razor"
       
    private int currentCount = 0;

    private void IncrementCount()
    {
        var random = new Random();
        currentCount++;
    }

        protected override void OnInitialized() => base.OnInitialized();


#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
