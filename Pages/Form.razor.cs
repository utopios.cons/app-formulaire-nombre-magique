﻿using System;
using Microsoft.AspNetCore.Components;
using nombre_magique.Pages;

namespace nombre_magique.Pages
{
    public class FormBase : ComponentBase
    {
        protected bool display = false;

        public Customer cust = new Customer() { birthdate = DateTime.Now.AddYears(-1) };

        public void OnFormValid()
        {
            Console.WriteLine("FORM SUBMITTED; GOOD JOB");

            display = true;
          

        }

    }
}
