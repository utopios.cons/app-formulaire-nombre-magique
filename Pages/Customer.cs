﻿using System;
using System.ComponentModel.DataAnnotations; 

namespace nombre_magique.Pages
{
	
	public class Customer
	{
		public string id { get; set; }

		[Required(ErrorMessage = "Saisie du nom requise.")]
		[StringLength(50, ErrorMessage = "Name must be no more than 50 characters.")]
		public string name { get; set; }

		[Required(ErrorMessage = "L'Adresse est requise.")]
		[StringLength(100, ErrorMessage = "L'adresse doit est supérieur à 100 characters.")]
		public string address { get; set; }

		[Required(ErrorMessage = "Le code postal est requis.")]
		[StringLength(10, ErrorMessage = "Le code postal must be no more than 10 characters.")]
		public string zip { get; set; }

		[Required(ErrorMessage = "Email est requis.")]
		[EmailAddress(ErrorMessage = "Email is not a valid email address.")]
		[StringLength(100, ErrorMessage = "Email must be no more than 100 characters.")]
		public string email { get; set; }

		[Required(ErrorMessage = "Age est requis.")]
		[Range(1, 125, ErrorMessage = "L'age doit être compris entre 1 et 125.")]
		public int age { get; set; }

		[Required(ErrorMessage = "Date de naissance est requise.")]
		public DateTime birthdate { get; set; }

		[Required(ErrorMessage = "Marié ou pas est requis.")]
		public bool married { get; set; }

		[Required(ErrorMessage = "La couleur est requise.")]
		public string color { get; set; }
	}
}

